package com.alpenite.factories;

import com.alpenite.Vehicle;

public abstract class VehicleFactory {
    public abstract Vehicle buildCar(String licencePlate);
    public abstract Vehicle buildBike(String licencePlate);
    public abstract Vehicle buildTruck(String licencePlate);
}
