package com.alpenite;

import java.util.ArrayList;
import java.util.List;

public class CarDealer implements CarDealerRules {
    private List<Vehicle> vehicles = new ArrayList<>();
    private String id;


    CarDealer(String id) {
        this.id = id;
    }

    List<Vehicle> getVehicles() {
        return vehicles;
    }

    void addVehicle(Vehicle vehicle) {
        if (!vehicles.contains(vehicle)) {
            vehicles.add(vehicle);
        }
    }

    @Override
    public String getId() {
        return id;
    }
}
