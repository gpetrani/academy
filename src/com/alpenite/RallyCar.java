package com.alpenite;

public class RallyCar extends Car {
    //licencePlate ereditata
    private VwGroup MANUFACTURER;

    RallyCar(String licencePlate, VwGroup MANUFACTURER) {
        super(licencePlate);
        this.MANUFACTURER = MANUFACTURER;
    }

    @Override
    public double getPerformance() {
        final double PERFORMANCE_ENHANCER = 1.5;
        double performance = super.getPerformance();
        return PERFORMANCE_ENHANCER * performance;
    }

    public VwGroup getMANUFACTURER() {
        return MANUFACTURER;
    }

    @Override
    public String toString() {
        return "RallyCar{" +
                "licencePlate='" + getLicencePlate() + '\'' +
                ", performance='" + getPerformance() + '\'' +
                '}';
    }
}
