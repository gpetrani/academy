package com.alpenite;

public class Car extends Vehicle {
    private String color;
    private int numberOfDoor;

    Car(String licencePlate) {
        super(licencePlate);
    }

    public String getSecurityDevices() {
        return "ABS, Seatbelts, Airbag";
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNumberOfDoor() {
        return numberOfDoor;
    }

    public void setNumberOfDoor(int numberOfDoor) {
        this.numberOfDoor = numberOfDoor;
    }


    // @Override
    // public String toString() {
    //     return "Car{" +
    //             "licencePlate='" + getLicencePlate() + '\'' +
    //             ", performance='" + getPerformance() + '\'' +
    //             ", color='" + getColor() + '\'' +
    //             ", numberOfDoor=" + getNumberOfDoor() +
    //             ", mass=" + getMass() +
    //             ", displacement=" + getDisplacement() +
    //             ", security devices=" + getSecurityDevices() +
    //             '}';
    // }


    @Override
    public String toString() {
        return "Car{licencePlate='" + getLicencePlate() + '\'' +
                ", performance='" + getPerformance() + '\'' +
                '}';
    }
}
