package com.alpenite;

public class Truck extends Vehicle {

    public Truck(String licencePlate) {
        super(licencePlate);
    }

    @Override
    public String getSecurityDevices() {
        return "Collision Detector, ABS, Seatbelts, Airbag";
    }

    @Override
    public double getPerformance() {
        final double PERFORMANCE_ENHANCER = 0.7;
        double performance = super.getPerformance();
        return PERFORMANCE_ENHANCER * performance;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "licencePlate='" + getLicencePlate() + '\'' +
                ", performance='" + this.getPerformance() + '\'' +
                '}';
    }
}
