package com.alpenite;

public class Bike extends Vehicle {

    Bike(String licencePlate) {
        super(licencePlate);
    }

    public String getSecurityDevices() {
        return "ABS, Helmet";
    }

    @Override
    public String toString() {
        return "Bike{" +
                "licencePlate='" + getLicencePlate() + '\'' +
                ", performance='" + this.getPerformance() + '\'' +
                '}';
    }
}
