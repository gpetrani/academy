package com.alpenite.utils;

public class PerformanceUtilities {
    private static final double CV_TO_KW = 0.735499;

    public static double getKwFromCv(double cv) {
        return cv * CV_TO_KW;
    }

}

// aggiungere proprieta' powerInCv ai veicoli, aggiungere la potenza ad ogni veicolo e stampare i kw