package com.alpenite;

import com.alpenite.utils.PerformanceUtilities;
import com.alpenite.factories.VehicleFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Main {


    public static void main(String[] args) {

        final String importFile = "import.txt";
        final List<String> importedVehicles = new ArrayList<>();
        final Map<String,String> importedVehiclesMap = new HashMap<>();
        loadFileToMap(importFile, importedVehicles, importedVehiclesMap);
        readFile(importFile);


        VehicleFactory polandVehicleFactory = new PolandVehicleFactory();
        Vehicle car1 = polandVehicleFactory.buildCar("AB000CD");
        Vehicle car2 = polandVehicleFactory.buildCar("AB000CD");
        Vehicle bike = polandVehicleFactory.buildBike("ZA123BC");
        Vehicle truck = polandVehicleFactory.buildTruck("FB987NM");


        GermanVehicleFactory germanVehicleFactory = new GermanVehicleFactory();
        Vehicle rallyCar1 = germanVehicleFactory.buildRallyCar("DB100AZ", VwGroup.PORSCHE);


        CarDealer carDealer1 = new CarDealer("Cars GmbH");
        CarDealer carDealer2 = new CarDealer("Sport Cars spa");

        car1.setDisplacement(1600);
        car1.setMass(1300);
        rallyCar1.setDisplacement(1600);
        rallyCar1.setMass(1350);
        rallyCar1.setPowerInCv(345);
        bike.setDisplacement(600);
        bike.setMass(180);
        truck.setDisplacement(4600);
        truck.setMass(6000);

        carDealer1.addVehicle(rallyCar1);
        carDealer1.addVehicle(car1);
        carDealer1.addVehicle(car2); //non la inserisce perche' stessa targa
        carDealer1.addVehicle(bike);
        carDealer1.addVehicle(truck);

        Collections.sort(carDealer1.getVehicles());

        for (Vehicle vehicle : carDealer1.getVehicles()) {
            double powerInKw = PerformanceUtilities.getKwFromCv(vehicle.getPowerInCv());
            System.out.println(vehicle.getLicencePlate() + ". Potenza in Kw: " + powerInKw);
        }


        //trovare chi ha, in media, le auto piu' performanti tra i due dealer
        //escludendo i truck dal calcolo
    }

    private static void loadFileToMap(String file, List<String> list, Map<String, String> map) {
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(file));

            String line;
            while ((line = reader.readLine()) != null) {
                //istanziazione dei veicoli
                list.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // for (String importedLine : list) {
        //     //save to HashMap
        //     String[] line = importedLine.split(",");
        //     map.put(line[1], line[0]); //evita doppioni
        // }
    }

    private static void readFile(String importFile) {
        Path pathCustomers = FileSystems.getDefault().getPath("import.txt");
        List<String> customers = new LinkedList<>();
        try {
            customers = Files.readAllLines(pathCustomers, Charset.defaultCharset());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        for (String line : customers) {
            System.out.println(line);
        }
    }
}
