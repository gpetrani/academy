package com.alpenite;

import java.util.Objects;

abstract public class Vehicle implements Comparable<Vehicle> {
    private double mass;
    private double displacement;
    private String licencePlate;

    private double powerInCv;

    Vehicle(String licencePlate) {
        this.licencePlate = licencePlate; //shadowing, non e' necessario inventarsi troppi nomi
    }

    abstract public String getSecurityDevices();

    public double getPerformance() throws ArithmeticException {
        return displacement / mass;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    double getMass() {
        return mass;
    }

    void setMass(double mass) {
        this.mass = mass;
    }

    double getDisplacement() {
        return displacement;
    }

    void setDisplacement(double displacement) {
        this.displacement = displacement;
    }

    public double getPowerInCv() {
        return powerInCv;
    }

    public void setPowerInCv(double powerInCv) {
        this.powerInCv = powerInCv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(licencePlate, vehicle.licencePlate);
    }

    @Override
    public int compareTo(Vehicle o) {
        if (this.getPerformance() > o.getPerformance()) {
            return 1;
        } else if (this.getPerformance() == o.getPerformance()) {
            return 0;
        } else if (this.getPerformance() < o.getPerformance()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "licencePlate='" + licencePlate + '\'' +
                ", performance='" + this.getPerformance() + '\'' +
                '}';
    }
}
