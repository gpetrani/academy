package com.alpenite;

public enum VwGroup {
    VW {
        @Override
        public String toString() {
            return "Volkswagen";
        }
    },
    PORSCHE {
        @Override
        public String toString() {
            return "Porsche";
        }
    },
    LAMBORGHINI,
    SEAT,
    SKODA,
    BENTLEY,
    BUGATTI,
    AUDI,
    DUCATI,
    SCANIA,
    MAN
}

// con
// for( VwGroup d : VwGroup.values() ) {
//  controlliamo che l'auto sia di questo gruppo di marchi
// }
