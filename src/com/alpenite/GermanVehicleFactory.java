package com.alpenite;

import com.alpenite.factories.VehicleFactory;

public class GermanVehicleFactory extends VehicleFactory {
    @Override
    public Vehicle buildCar(String licencePlate) {
        return new Car(licencePlate);
    }

    @Override
    public Vehicle buildBike(String licencePlate) {
        return new Bike(licencePlate);
    }

    @Override
    public Vehicle buildTruck(String licencePlate) {
        return new Truck(licencePlate);
    }

    public Vehicle buildRallyCar(String licencePlate, VwGroup brand) {
        return new RallyCar(licencePlate, brand);
    }
}
